/// cod : "200"
/// message : 0
/// cnt : 40
/// list : [{"dt":1663869600,"main":{"temp":302.07,"feels_like":306.26,"temp_min":299.94,"temp_max":302.07,"pressure":1007,"sea_level":1007,"grnd_level":1005,"humidity":73,"temp_kf":2.13},"weather":[{"id":500,"main":"Rain","description":"light rain","icon":"10n"}],"clouds":{"all":37},"wind":{"speed":3.12,"deg":192,"gust":5.95},"visibility":10000,"pop":0.25,"rain":{"3h":0.12},"sys":{"pod":"n"},"dt_txt":"2022-09-22 18:00:00"},{"dt":1663880400,"main":{"temp":300.61,"feels_like":304.06,"temp_min":299.34,"temp_max":300.61,"pressure":1006,"sea_level":1006,"grnd_level":1004,"humidity":81,"temp_kf":1.27},"weather":[{"id":803,"main":"Clouds","description":"broken clouds","icon":"04n"}],"clouds":{"all":63},"wind":{"speed":2.48,"deg":198,"gust":4.05},"visibility":10000,"pop":0.18,"sys":{"pod":"n"},"dt_txt":"2022-09-22 21:00:00"},{"dt":1663891200,"main":{"temp":298.98,"feels_like":299.97,"temp_min":298.98,"temp_max":298.98,"pressure":1007,"sea_level":1007,"grnd_level":1005,"humidity":90,"temp_kf":0},"weather":[{"id":803,"main":"Clouds","description":"broken clouds","icon":"04d"}],"clouds":{"all":84},"wind":{"speed":2.1,"deg":161,"gust":2.69},"visibility":10000,"pop":0.27,"sys":{"pod":"d"},"dt_txt":"2022-09-23 00:00:00"},{"dt":1663902000,"main":{"temp":303.42,"feels_like":308.35,"temp_min":303.42,"temp_max":303.42,"pressure":1008,"sea_level":1008,"grnd_level":1007,"humidity":68,"temp_kf":0},"weather":[{"id":803,"main":"Clouds","description":"broken clouds","icon":"04d"}],"clouds":{"all":53},"wind":{"speed":2.78,"deg":165,"gust":3.26},"visibility":10000,"pop":0.39,"sys":{"pod":"d"},"dt_txt":"2022-09-23 03:00:00"},{"dt":1663912800,"main":{"temp":306.62,"feels_like":311.37,"temp_min":306.62,"temp_max":306.62,"pressure":1006,"sea_level":1006,"grnd_level":1004,"humidity":53,"temp_kf":0},"weather":[{"id":500,"main":"Rain","description":"light rain","icon":"10d"}],"clouds":{"all":69},"wind":{"speed":2.99,"deg":181,"gust":3.2},"visibility":10000,"pop":0.36,"rain":{"3h":0.12},"sys":{"pod":"d"},"dt_txt":"2022-09-23 06:00:00"},{"dt":1663923600,"main":{"temp":305.87,"feels_like":310.96,"temp_min":305.87,"temp_max":305.87,"pressure":1003,"sea_level":1003,"grnd_level":1001,"humidity":57,"temp_kf":0},"weather":[{"id":500,"main":"Rain","description":"light rain","icon":"10d"}],"clouds":{"all":74},"wind":{"speed":2.15,"deg":188,"gust":3.25},"visibility":10000,"pop":0.21,"rain":{"3h":0.12},"sys":{"pod":"d"},"dt_txt":"2022-09-23 09:00:00"},{"dt":1663934400,"main":{"temp":303.15,"feels_like":307.48,"temp_min":303.15,"temp_max":303.15,"pressure":1004,"sea_level":1004,"grnd_level":1002,"humidity":67,"temp_kf":0},"weather":[{"id":500,"main":"Rain","description":"light rain","icon":"10n"}],"clouds":{"all":85},"wind":{"speed":3.22,"deg":171,"gust":4.59},"visibility":10000,"pop":0.26,"rain":{"3h":0.16},"sys":{"pod":"n"},"dt_txt":"2022-09-23 12:00:00"},{"dt":1663945200,"main":{"temp":301.46,"feels_like":305.2,"temp_min":301.46,"temp_max":301.46,"pressure":1006,"sea_level":1006,"grnd_level":1005,"humidity":75,"temp_kf":0},"weather":[{"id":804,"main":"Clouds","description":"overcast clouds","icon":"04n"}],"clouds":{"all":97},"wind":{"speed":3.78,"deg":171,"gust":6.82},"visibility":10000,"pop":0,"sys":{"pod":"n"},"dt_txt":"2022-09-23 15:00:00"},{"dt":1663956000,"main":{"temp":300.16,"feels_like":303.1,"temp_min":300.16,"temp_max":300.16,"pressure":1006,"sea_level":1006,"grnd_level":1005,"humidity":82,"temp_kf":0},"weather":[{"id":804,"main":"Clouds","description":"overcast clouds","icon":"04n"}],"clouds":{"all":99},"wind":{"speed":3.59,"deg":203,"gust":6.84},"visibility":10000,"pop":0,"sys":{"pod":"n"},"dt_txt":"2022-09-23 18:00:00"},{"dt":1663966800,"main":{"temp":299.35,"feels_like":299.35,"temp_min":299.35,"temp_max":299.35,"pressure":1005,"sea_level":1005,"grnd_level":1004,"humidity":87,"temp_kf":0},"weather":[{"id":500,"main":"Rain","description":"light rain","icon":"10n"}],"clouds":{"all":100},"wind":{"speed":3.19,"deg":203,"gust":6.07},"visibility":10000,"pop":0.42,"rain":{"3h":0.99},"sys":{"pod":"n"},"dt_txt":"2022-09-23 21:00:00"},{"dt":1663977600,"main":{"temp":298.79,"feels_like":299.76,"temp_min":298.79,"temp_max":298.79,"pressure":1007,"sea_level":1007,"grnd_level":1005,"humidity":90,"temp_kf":0},"weather":[{"id":803,"main":"Clouds","description":"broken clouds","icon":"04d"}],"clouds":{"all":82},"wind":{"speed":2.45,"deg":199,"gust":4.2},"visibility":10000,"pop":0.33,"sys":{"pod":"d"},"dt_txt":"2022-09-24 00:00:00"},{"dt":1663988400,"main":{"temp":303.25,"feels_like":307.71,"temp_min":303.25,"temp_max":303.25,"pressure":1008,"sea_level":1008,"grnd_level":1006,"humidity":67,"temp_kf":0},"weather":[{"id":802,"main":"Clouds","description":"scattered clouds","icon":"03d"}],"clouds":{"all":33},"wind":{"speed":3.02,"deg":187,"gust":3.57},"visibility":10000,"pop":0.12,"sys":{"pod":"d"},"dt_txt":"2022-09-24 03:00:00"},{"dt":1663999200,"main":{"temp":306.57,"feels_like":310,"temp_min":306.57,"temp_max":306.57,"pressure":1006,"sea_level":1006,"grnd_level":1004,"humidity":49,"temp_kf":0},"weather":[{"id":801,"main":"Clouds","description":"few clouds","icon":"02d"}],"clouds":{"all":23},"wind":{"speed":3.1,"deg":184,"gust":3.16},"visibility":10000,"pop":0.12,"sys":{"pod":"d"},"dt_txt":"2022-09-24 06:00:00"},{"dt":1664010000,"main":{"temp":307.12,"feels_like":309.9,"temp_min":307.12,"temp_max":307.12,"pressure":1003,"sea_level":1003,"grnd_level":1001,"humidity":45,"temp_kf":0},"weather":[{"id":801,"main":"Clouds","description":"few clouds","icon":"02d"}],"clouds":{"all":22},"wind":{"speed":3.11,"deg":184,"gust":2.64},"visibility":10000,"pop":0,"sys":{"pod":"d"},"dt_txt":"2022-09-24 09:00:00"},{"dt":1664020800,"main":{"temp":303.42,"feels_like":306.09,"temp_min":303.42,"temp_max":303.42,"pressure":1003,"sea_level":1003,"grnd_level":1002,"humidity":58,"temp_kf":0},"weather":[{"id":803,"main":"Clouds","description":"broken clouds","icon":"04n"}],"clouds":{"all":59},"wind":{"speed":3.36,"deg":159,"gust":4.5},"visibility":10000,"pop":0,"sys":{"pod":"n"},"dt_txt":"2022-09-24 12:00:00"},{"dt":1664031600,"main":{"temp":301.29,"feels_like":304.67,"temp_min":301.29,"temp_max":301.29,"pressure":1006,"sea_level":1006,"grnd_level":1004,"humidity":74,"temp_kf":0},"weather":[{"id":804,"main":"Clouds","description":"overcast clouds","icon":"04n"}],"clouds":{"all":100},"wind":{"speed":4.27,"deg":181,"gust":7.62},"visibility":10000,"pop":0.01,"sys":{"pod":"n"},"dt_txt":"2022-09-24 15:00:00"},{"dt":1664042400,"main":{"temp":300.32,"feels_like":303.48,"temp_min":300.32,"temp_max":300.32,"pressure":1006,"sea_level":1006,"grnd_level":1004,"humidity":82,"temp_kf":0},"weather":[{"id":804,"main":"Clouds","description":"overcast clouds","icon":"04n"}],"clouds":{"all":99},"wind":{"speed":2.82,"deg":191,"gust":5.02},"visibility":10000,"pop":0.04,"sys":{"pod":"n"},"dt_txt":"2022-09-24 18:00:00"},{"dt":1664053200,"main":{"temp":299.77,"feels_like":299.77,"temp_min":299.77,"temp_max":299.77,"pressure":1005,"sea_level":1005,"grnd_level":1003,"humidity":85,"temp_kf":0},"weather":[{"id":804,"main":"Clouds","description":"overcast clouds","icon":"04n"}],"clouds":{"all":100},"wind":{"speed":2.4,"deg":189,"gust":3.92},"visibility":10000,"pop":0.11,"sys":{"pod":"n"},"dt_txt":"2022-09-24 21:00:00"},{"dt":1664064000,"main":{"temp":299.43,"feels_like":299.43,"temp_min":299.43,"temp_max":299.43,"pressure":1006,"sea_level":1006,"grnd_level":1004,"humidity":87,"temp_kf":0},"weather":[{"id":804,"main":"Clouds","description":"overcast clouds","icon":"04d"}],"clouds":{"all":100},"wind":{"speed":2.11,"deg":175,"gust":3.09},"visibility":10000,"pop":0.14,"sys":{"pod":"d"},"dt_txt":"2022-09-25 00:00:00"},{"dt":1664074800,"main":{"temp":303.22,"feels_like":307.64,"temp_min":303.22,"temp_max":303.22,"pressure":1008,"sea_level":1008,"grnd_level":1006,"humidity":67,"temp_kf":0},"weather":[{"id":803,"main":"Clouds","description":"broken clouds","icon":"04d"}],"clouds":{"all":81},"wind":{"speed":2.44,"deg":163,"gust":2.88},"visibility":10000,"pop":0.19,"sys":{"pod":"d"},"dt_txt":"2022-09-25 03:00:00"},{"dt":1664085600,"main":{"temp":306.71,"feels_like":310.28,"temp_min":306.71,"temp_max":306.71,"pressure":1006,"sea_level":1006,"grnd_level":1005,"humidity":49,"temp_kf":0},"weather":[{"id":803,"main":"Clouds","description":"broken clouds","icon":"04d"}],"clouds":{"all":74},"wind":{"speed":2.77,"deg":164,"gust":2.88},"visibility":10000,"pop":0.16,"sys":{"pod":"d"},"dt_txt":"2022-09-25 06:00:00"},{"dt":1664096400,"main":{"temp":306.76,"feels_like":310.39,"temp_min":306.76,"temp_max":306.76,"pressure":1003,"sea_level":1003,"grnd_level":1002,"humidity":49,"temp_kf":0},"weather":[{"id":804,"main":"Clouds","description":"overcast clouds","icon":"04d"}],"clouds":{"all":98},"wind":{"speed":2.88,"deg":155,"gust":2.44},"visibility":10000,"pop":0.3,"sys":{"pod":"d"},"dt_txt":"2022-09-25 09:00:00"},{"dt":1664107200,"main":{"temp":302.93,"feels_like":306.99,"temp_min":302.93,"temp_max":302.93,"pressure":1005,"sea_level":1005,"grnd_level":1003,"humidity":67,"temp_kf":0},"weather":[{"id":803,"main":"Clouds","description":"broken clouds","icon":"04n"}],"clouds":{"all":83},"wind":{"speed":4.02,"deg":157,"gust":6.42},"visibility":10000,"pop":0.37,"sys":{"pod":"n"},"dt_txt":"2022-09-25 12:00:00"},{"dt":1664118000,"main":{"temp":301.2,"feels_like":305.06,"temp_min":301.2,"temp_max":301.2,"pressure":1008,"sea_level":1008,"grnd_level":1006,"humidity":78,"temp_kf":0},"weather":[{"id":500,"main":"Rain","description":"light rain","icon":"10n"}],"clouds":{"all":76},"wind":{"speed":3.05,"deg":164,"gust":4.84},"visibility":10000,"pop":0.48,"rain":{"3h":0.13},"sys":{"pod":"n"},"dt_txt":"2022-09-25 15:00:00"},{"dt":1664128800,"main":{"temp":299.48,"feels_like":299.48,"temp_min":299.48,"temp_max":299.48,"pressure":1008,"sea_level":1008,"grnd_level":1006,"humidity":87,"temp_kf":0},"weather":[{"id":501,"main":"Rain","description":"moderate rain","icon":"10n"}],"clouds":{"all":88},"wind":{"speed":2.47,"deg":185,"gust":4.76},"visibility":10000,"pop":0.59,"rain":{"3h":3.39},"sys":{"pod":"n"},"dt_txt":"2022-09-25 18:00:00"},{"dt":1664139600,"main":{"temp":299.42,"feels_like":299.42,"temp_min":299.42,"temp_max":299.42,"pressure":1006,"sea_level":1006,"grnd_level":1004,"humidity":87,"temp_kf":0},"weather":[{"id":500,"main":"Rain","description":"light rain","icon":"10n"}],"clouds":{"all":100},"wind":{"speed":2.02,"deg":204,"gust":3.16},"visibility":10000,"pop":0.73,"rain":{"3h":2.76},"sys":{"pod":"n"},"dt_txt":"2022-09-25 21:00:00"},{"dt":1664150400,"main":{"temp":298.77,"feels_like":299.74,"temp_min":298.77,"temp_max":298.77,"pressure":1007,"sea_level":1007,"grnd_level":1005,"humidity":90,"temp_kf":0},"weather":[{"id":500,"main":"Rain","description":"light rain","icon":"10d"}],"clouds":{"all":100},"wind":{"speed":2.27,"deg":136,"gust":3.62},"visibility":10000,"pop":0.77,"rain":{"3h":2.1},"sys":{"pod":"d"},"dt_txt":"2022-09-26 00:00:00"},{"dt":1664161200,"main":{"temp":301.88,"feels_like":305.82,"temp_min":301.88,"temp_max":301.88,"pressure":1009,"sea_level":1009,"grnd_level":1008,"humidity":73,"temp_kf":0},"weather":[{"id":500,"main":"Rain","description":"light rain","icon":"10d"}],"clouds":{"all":75},"wind":{"speed":3.61,"deg":115,"gust":3.83},"visibility":10000,"pop":0.82,"rain":{"3h":1.33},"sys":{"pod":"d"},"dt_txt":"2022-09-26 03:00:00"},{"dt":1664172000,"main":{"temp":304.87,"feels_like":309.29,"temp_min":304.87,"temp_max":304.87,"pressure":1007,"sea_level":1007,"grnd_level":1006,"humidity":59,"temp_kf":0},"weather":[{"id":803,"main":"Clouds","description":"broken clouds","icon":"04d"}],"clouds":{"all":71},"wind":{"speed":2.96,"deg":115,"gust":2.2},"visibility":10000,"pop":0.76,"sys":{"pod":"d"},"dt_txt":"2022-09-26 06:00:00"},{"dt":1664182800,"main":{"temp":304.03,"feels_like":308.76,"temp_min":304.03,"temp_max":304.03,"pressure":1005,"sea_level":1005,"grnd_level":1003,"humidity":64,"temp_kf":0},"weather":[{"id":500,"main":"Rain","description":"light rain","icon":"10d"}],"clouds":{"all":95},"wind":{"speed":2.19,"deg":115,"gust":2.02},"visibility":10000,"pop":0.77,"rain":{"3h":1.59},"sys":{"pod":"d"},"dt_txt":"2022-09-26 09:00:00"},{"dt":1664193600,"main":{"temp":301.81,"feels_like":306.02,"temp_min":301.81,"temp_max":301.81,"pressure":1005,"sea_level":1005,"grnd_level":1003,"humidity":75,"temp_kf":0},"weather":[{"id":500,"main":"Rain","description":"light rain","icon":"10n"}],"clouds":{"all":92},"wind":{"speed":2.71,"deg":131,"gust":3.46},"visibility":10000,"pop":0.81,"rain":{"3h":0.77},"sys":{"pod":"n"},"dt_txt":"2022-09-26 12:00:00"},{"dt":1664204400,"main":{"temp":300.4,"feels_like":303.67,"temp_min":300.4,"temp_max":300.4,"pressure":1007,"sea_level":1007,"grnd_level":1005,"humidity":82,"temp_kf":0},"weather":[{"id":500,"main":"Rain","description":"light rain","icon":"10n"}],"clouds":{"all":98},"wind":{"speed":2.25,"deg":149,"gust":3.33},"visibility":10000,"pop":0.52,"rain":{"3h":2.43},"sys":{"pod":"n"},"dt_txt":"2022-09-26 15:00:00"},{"dt":1664215200,"main":{"temp":299.93,"feels_like":302.83,"temp_min":299.93,"temp_max":299.93,"pressure":1007,"sea_level":1007,"grnd_level":1005,"humidity":85,"temp_kf":0},"weather":[{"id":804,"main":"Clouds","description":"overcast clouds","icon":"04n"}],"clouds":{"all":99},"wind":{"speed":2.66,"deg":179,"gust":4.38},"visibility":10000,"pop":0.44,"sys":{"pod":"n"},"dt_txt":"2022-09-26 18:00:00"},{"dt":1664226000,"main":{"temp":299.29,"feels_like":299.29,"temp_min":299.29,"temp_max":299.29,"pressure":1005,"sea_level":1005,"grnd_level":1004,"humidity":86,"temp_kf":0},"weather":[{"id":500,"main":"Rain","description":"light rain","icon":"10n"}],"clouds":{"all":100},"wind":{"speed":2.57,"deg":183,"gust":4.06},"visibility":10000,"pop":0.53,"rain":{"3h":1.23},"sys":{"pod":"n"},"dt_txt":"2022-09-26 21:00:00"},{"dt":1664236800,"main":{"temp":299.02,"feels_like":299.93,"temp_min":299.02,"temp_max":299.02,"pressure":1007,"sea_level":1007,"grnd_level":1005,"humidity":87,"temp_kf":0},"weather":[{"id":500,"main":"Rain","description":"light rain","icon":"10d"}],"clouds":{"all":100},"wind":{"speed":2.3,"deg":173,"gust":3.61},"visibility":10000,"pop":0.74,"rain":{"3h":1.42},"sys":{"pod":"d"},"dt_txt":"2022-09-27 00:00:00"},{"dt":1664247600,"main":{"temp":303.02,"feels_like":307.41,"temp_min":303.02,"temp_max":303.02,"pressure":1008,"sea_level":1008,"grnd_level":1007,"humidity":68,"temp_kf":0},"weather":[{"id":500,"main":"Rain","description":"light rain","icon":"10d"}],"clouds":{"all":90},"wind":{"speed":2.13,"deg":183,"gust":2.41},"visibility":10000,"pop":0.78,"rain":{"3h":0.93},"sys":{"pod":"d"},"dt_txt":"2022-09-27 03:00:00"},{"dt":1664258400,"main":{"temp":305.42,"feels_like":309.94,"temp_min":305.42,"temp_max":305.42,"pressure":1006,"sea_level":1006,"grnd_level":1004,"humidity":57,"temp_kf":0},"weather":[{"id":500,"main":"Rain","description":"light rain","icon":"10d"}],"clouds":{"all":94},"wind":{"speed":2.19,"deg":139,"gust":2.12},"visibility":10000,"pop":0.74,"rain":{"3h":0.73},"sys":{"pod":"d"},"dt_txt":"2022-09-27 06:00:00"},{"dt":1664269200,"main":{"temp":305.02,"feels_like":310.22,"temp_min":305.02,"temp_max":305.02,"pressure":1003,"sea_level":1003,"grnd_level":1002,"humidity":61,"temp_kf":0},"weather":[{"id":500,"main":"Rain","description":"light rain","icon":"10d"}],"clouds":{"all":100},"wind":{"speed":2.45,"deg":134,"gust":2.15},"visibility":10000,"pop":0.29,"rain":{"3h":0.15},"sys":{"pod":"d"},"dt_txt":"2022-09-27 09:00:00"},{"dt":1664280000,"main":{"temp":302.47,"feels_like":306.79,"temp_min":302.47,"temp_max":302.47,"pressure":1003,"sea_level":1003,"grnd_level":1002,"humidity":71,"temp_kf":0},"weather":[{"id":500,"main":"Rain","description":"light rain","icon":"10n"}],"clouds":{"all":80},"wind":{"speed":2.34,"deg":144,"gust":2.99},"visibility":10000,"pop":0.28,"rain":{"3h":0.85},"sys":{"pod":"n"},"dt_txt":"2022-09-27 12:00:00"},{"dt":1664290800,"main":{"temp":300.94,"feels_like":304.45,"temp_min":300.94,"temp_max":300.94,"pressure":1006,"sea_level":1006,"grnd_level":1004,"humidity":78,"temp_kf":0},"weather":[{"id":500,"main":"Rain","description":"light rain","icon":"10n"}],"clouds":{"all":26},"wind":{"speed":2.87,"deg":151,"gust":4.19},"visibility":10000,"pop":0.24,"rain":{"3h":0.11},"sys":{"pod":"n"},"dt_txt":"2022-09-27 15:00:00"}]
/// city : {"id":1188909,"name":"Sāmāir","coord":{"lat":23.8103,"lon":90.4125},"country":"BD","population":0,"timezone":21600,"sunrise":1663804012,"sunset":1663847740}

class WeatherForcast {
  WeatherForcast({
      this.cod, 
      this.message, 
      this.cnt, 
      this.list, 
      this.city,});

  WeatherForcast.fromJson(dynamic json) {
    cod = json['cod'];
    message = json['message'];
    cnt = json['cnt'];
    if (json['list'] != null) {
      list = [];
      json['list'].forEach((v) {
        list?.add(ForecastList.fromJson(v));
      });
    }
    city = json['city'] != null ? City.fromJson(json['city']) : null;
  }
  String? cod;
  num? message;
  num? cnt;
  List<ForecastList>? list;
  City? city;
    WeatherForcast copyWith({  String? cod,
  num? message,
  num? cnt,
  List<ForecastList>? list,
  City? city,
}) => WeatherForcast(  cod: cod ?? this.cod,
  message: message ?? this.message,
  cnt: cnt ?? this.cnt,
  list: list ?? this.list,
  city: city ?? this.city,
);
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['cod'] = cod;
    map['message'] = message;
    map['cnt'] = cnt;
    if (list != null) {
      map['list'] = list?.map((v) => v.toJson()).toList();
    }
    if (city != null) {
      map['city'] = city?.toJson();
    }
    return map;
  }

}

/// id : 1188909
/// name : "Sāmāir"
/// coord : {"lat":23.8103,"lon":90.4125}
/// country : "BD"
/// population : 0
/// timezone : 21600
/// sunrise : 1663804012
/// sunset : 1663847740

class City {
  City({
      this.id, 
      this.name, 
      this.coord, 
      this.country, 
      this.population, 
      this.timezone, 
      this.sunrise, 
      this.sunset,});

  City.fromJson(dynamic json) {
    id = json['id'];
    name = json['name'];
    coord = json['coord'] != null ? Coord.fromJson(json['coord']) : null;
    country = json['country'];
    population = json['population'];
    timezone = json['timezone'];
    sunrise = json['sunrise'];
    sunset = json['sunset'];
  }
  num? id;
  String? name;
  Coord? coord;
  String? country;
  num? population;
  num? timezone;
  num? sunrise;
  num? sunset;
City copyWith({  num? id,
  String? name,
  Coord? coord,
  String? country,
  num? population,
  num? timezone,
  num? sunrise,
  num? sunset,
}) => City(  id: id ?? this.id,
  name: name ?? this.name,
  coord: coord ?? this.coord,
  country: country ?? this.country,
  population: population ?? this.population,
  timezone: timezone ?? this.timezone,
  sunrise: sunrise ?? this.sunrise,
  sunset: sunset ?? this.sunset,
);
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['name'] = name;
    if (coord != null) {
      map['coord'] = coord?.toJson();
    }
    map['country'] = country;
    map['population'] = population;
    map['timezone'] = timezone;
    map['sunrise'] = sunrise;
    map['sunset'] = sunset;
    return map;
  }

}

/// lat : 23.8103
/// lon : 90.4125

class Coord {
  Coord({
      this.lat, 
      this.lon,});

  Coord.fromJson(dynamic json) {
    lat = json['lat'];
    lon = json['lon'];
  }
  num? lat;
  num? lon;
Coord copyWith({  num? lat,
  num? lon,
}) => Coord(  lat: lat ?? this.lat,
  lon: lon ?? this.lon,
);
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['lat'] = lat;
    map['lon'] = lon;
    return map;
  }

}

/// dt : 1663869600
/// main : {"temp":302.07,"feels_like":306.26,"temp_min":299.94,"temp_max":302.07,"pressure":1007,"sea_level":1007,"grnd_level":1005,"humidity":73,"temp_kf":2.13}
/// weather : [{"id":500,"main":"Rain","description":"light rain","icon":"10n"}]
/// clouds : {"all":37}
/// wind : {"speed":3.12,"deg":192,"gust":5.95}
/// visibility : 10000
/// pop : 0.25
/// rain : {"3h":0.12}
/// sys : {"pod":"n"}
/// dt_txt : "2022-09-22 18:00:00"

class ForecastList {
  ForecastList({
      this.dt, 
      this.main, 
      this.weather, 
      this.clouds, 
      this.wind, 
      this.visibility, 
      this.pop, 
      this.rain, 
      this.sys, 
      this.dtTxt,});

  ForecastList.fromJson(dynamic json) {
    dt = json['dt'];
    main = json['main'] != null ? Main.fromJson(json['main']) : null;
    if (json['weather'] != null) {
      weather = [];
      json['weather'].forEach((v) {
        weather?.add(Weather.fromJson(v));
      });
    }
    clouds = json['clouds'] != null ? Clouds.fromJson(json['clouds']) : null;
    wind = json['wind'] != null ? Wind.fromJson(json['wind']) : null;
    visibility = json['visibility'];
    pop = json['pop'];
    rain = json['rain'] != null ? Rain.fromJson(json['rain']) : null;
    sys = json['sys'] != null ? Sys.fromJson(json['sys']) : null;
    dtTxt = json['dt_txt'];
  }
  num? dt;
  Main? main;
  List<Weather>? weather;
  Clouds? clouds;
  Wind? wind;
  num? visibility;
  num? pop;
  Rain? rain;
  Sys? sys;
  String? dtTxt;
ForecastList copyWith({  num? dt,
  Main? main,
  List<Weather>? weather,
  Clouds? clouds,
  Wind? wind,
  num? visibility,
  num? pop,
  Rain? rain,
  Sys? sys,
  String? dtTxt,
}) => ForecastList(  dt: dt ?? this.dt,
  main: main ?? this.main,
  weather: weather ?? this.weather,
  clouds: clouds ?? this.clouds,
  wind: wind ?? this.wind,
  visibility: visibility ?? this.visibility,
  pop: pop ?? this.pop,
  rain: rain ?? this.rain,
  sys: sys ?? this.sys,
  dtTxt: dtTxt ?? this.dtTxt,
);
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['dt'] = dt;
    if (main != null) {
      map['main'] = main?.toJson();
    }
    if (weather != null) {
      map['weather'] = weather?.map((v) => v.toJson()).toList();
    }
    if (clouds != null) {
      map['clouds'] = clouds?.toJson();
    }
    if (wind != null) {
      map['wind'] = wind?.toJson();
    }
    map['visibility'] = visibility;
    map['pop'] = pop;
    if (rain != null) {
      map['rain'] = rain?.toJson();
    }
    if (sys != null) {
      map['sys'] = sys?.toJson();
    }
    map['dt_txt'] = dtTxt;
    return map;
  }

}

/// pod : "n"

class Sys {
  Sys({
      this.pod,});

  Sys.fromJson(dynamic json) {
    pod = json['pod'];
  }
  String? pod;
Sys copyWith({  String? pod,
}) => Sys(  pod: pod ?? this.pod,
);
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['pod'] = pod;
    return map;
  }

}

/// 3h : 0.12

class Rain {
  Rain({
      this.h,});

  Rain.fromJson(dynamic json) {
    h = json['3h'];
  }
  num? h;
Rain copyWith({  num? h,
}) => Rain(  h: h ?? this.h,
);
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['3h'] = h;
    return map;
  }

}

/// speed : 3.12
/// deg : 192
/// gust : 5.95

class Wind {
  Wind({
      this.speed, 
      this.deg, 
      this.gust,});

  Wind.fromJson(dynamic json) {
    speed = json['speed'];
    deg = json['deg'];
    gust = json['gust'];
  }
  num? speed;
  num? deg;
  num? gust;
Wind copyWith({  num? speed,
  num? deg,
  num? gust,
}) => Wind(  speed: speed ?? this.speed,
  deg: deg ?? this.deg,
  gust: gust ?? this.gust,
);
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['speed'] = speed;
    map['deg'] = deg;
    map['gust'] = gust;
    return map;
  }

}

/// all : 37

class Clouds {
  Clouds({
      this.all,});

  Clouds.fromJson(dynamic json) {
    all = json['all'];
  }
  num? all;
Clouds copyWith({  num? all,
}) => Clouds(  all: all ?? this.all,
);
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['all'] = all;
    return map;
  }

}

/// id : 500
/// main : "Rain"
/// description : "light rain"
/// icon : "10n"

class Weather {
  Weather({
      this.id, 
      this.main, 
      this.description, 
      this.icon,});

  Weather.fromJson(dynamic json) {
    id = json['id'];
    main = json['main'];
    description = json['description'];
    icon = json['icon'];
  }
  num? id;
  String? main;
  String? description;
  String? icon;
Weather copyWith({  num? id,
  String? main,
  String? description,
  String? icon,
}) => Weather(  id: id ?? this.id,
  main: main ?? this.main,
  description: description ?? this.description,
  icon: icon ?? this.icon,
);
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['main'] = main;
    map['description'] = description;
    map['icon'] = icon;
    return map;
  }

}

/// temp : 302.07
/// feels_like : 306.26
/// temp_min : 299.94
/// temp_max : 302.07
/// pressure : 1007
/// sea_level : 1007
/// grnd_level : 1005
/// humidity : 73
/// temp_kf : 2.13

class Main {
  Main({
      this.temp, 
      this.feelsLike, 
      this.tempMin, 
      this.tempMax, 
      this.pressure, 
      this.seaLevel, 
      this.grndLevel, 
      this.humidity, 
      this.tempKf,});

  Main.fromJson(dynamic json) {
    temp = json['temp'];
    feelsLike = json['feels_like'];
    tempMin = json['temp_min'];
    tempMax = json['temp_max'];
    pressure = json['pressure'];
    seaLevel = json['sea_level'];
    grndLevel = json['grnd_level'];
    humidity = json['humidity'];
    tempKf = json['temp_kf'];
  }
  num? temp;
  num? feelsLike;
  num? tempMin;
  num? tempMax;
  num? pressure;
  num? seaLevel;
  num? grndLevel;
  num? humidity;
  num? tempKf;
Main copyWith({  num? temp,
  num? feelsLike,
  num? tempMin,
  num? tempMax,
  num? pressure,
  num? seaLevel,
  num? grndLevel,
  num? humidity,
  num? tempKf,
}) => Main(  temp: temp ?? this.temp,
  feelsLike: feelsLike ?? this.feelsLike,
  tempMin: tempMin ?? this.tempMin,
  tempMax: tempMax ?? this.tempMax,
  pressure: pressure ?? this.pressure,
  seaLevel: seaLevel ?? this.seaLevel,
  grndLevel: grndLevel ?? this.grndLevel,
  humidity: humidity ?? this.humidity,
  tempKf: tempKf ?? this.tempKf,
);
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['temp'] = temp;
    map['feels_like'] = feelsLike;
    map['temp_min'] = tempMin;
    map['temp_max'] = tempMax;
    map['pressure'] = pressure;
    map['sea_level'] = seaLevel;
    map['grnd_level'] = grndLevel;
    map['humidity'] = humidity;
    map['temp_kf'] = tempKf;
    return map;
  }

}