
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:weather_app/provider/weather_provider.dart';
import 'package:weather_app/utils/helper_function.dart';

import '../utils/constants.dart';

class HomePage extends StatefulWidget {
   static const  String routeName='homePage';
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {


  late WeatherProvider _weatherProvider;
  bool isInit=true;
  num? vi;
  @override
  void didChangeDependencies() {

     if(isInit){
       _weatherProvider=Provider.of<WeatherProvider>(context);
       determinePosition().then((position) {
         _weatherProvider.setPosition(position);
         _weatherProvider.getWeatherData();
         _weatherProvider.getWeatherForecastData();
         vi=_weatherProvider.weatherModel!.visibility;

         isInit=false;
       });
     }
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        title: const Text('Weather App',style: TextStyle(fontSize: 28),),
        actions: [
          IconButton(onPressed: (){}, icon: const Icon(Icons.settings,))
        ],
      ),
      body: _weatherProvider.weatherModel!=null && _weatherProvider.weatherForcast !=null ?Stack(
        children: [
          Image.asset('assets/img/img.png',
            width: double.infinity,
            height: double.infinity,
          fit: BoxFit.fill,),
           Center(
             
             child: ListView(
               padding: EdgeInsets.all(16.0),
               children: [
                 Column(
                   children: [
                     SizedBox(height: 100,),
                     Text(getFormatedDate(_weatherProvider.weatherModel!.dt!,'EEEE, MMM d, yyyy'),style: TextStyle(fontSize: 25),),
                     Text('${_weatherProvider.weatherModel!.name!},${_weatherProvider.weatherModel!.sys!.country!}',style: TextStyle(fontSize: 22),),
                     Text('${_weatherProvider.weatherModel!.main!.temp!.toStringAsFixed(1)}\u00B0',style: TextStyle(fontSize: 80),),
                     Text('feels like ${_weatherProvider.weatherModel!.main!.feelsLike!.round()}\u00B0',style: TextStyle(fontSize: 20),),
                     Row(
                       mainAxisAlignment: MainAxisAlignment.center,
                       children: [
                         Image.network('$iconPrefix${_weatherProvider.weatherModel!.weather![0].icon}$IconSufix'
                         ,height: 80,
                         width: 80,),
                         Text('${_weatherProvider.weatherModel!.weather![0].description}',style: TextStyle(fontSize: 20))
                       ],
                     ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text('Pressure: ${_weatherProvider.weatherModel!.main!.pressure!}mb',style: TextStyle(fontSize: 20),),
                          Text('Humidity:${_weatherProvider.weatherModel!.main!.humidity!}%',style: TextStyle(fontSize: 20),),
                        ],
                      ),
                     const SizedBox(height: 10,),


                     Row(
                       mainAxisAlignment: MainAxisAlignment.spaceBetween,
                       children: [
                         Text('Sunrise: ${getFormatedDate(_weatherProvider.weatherModel!.sys!.sunrise!,'HH:mm a ')}',style: TextStyle(fontSize: 20),),
                         Text('Sunset: ${getFormatedDate(_weatherProvider.weatherModel!.sys!.sunset!,'HH:mm a ')}',style: TextStyle(fontSize: 20),),
                       ],
                     ),
                     const SizedBox(height: 10,),
                     Row(
                       mainAxisAlignment: MainAxisAlignment.spaceBetween,
                       children: [
                         //Text(''),
                         Text('Visibility: ${getKm().toStringAsFixed(2)} km',style: TextStyle(fontSize: 20),),
                       ],
                     ),


                     SizedBox(height: 100,),
                     const Text('Next 5 Day / 3 Hour Forecast',style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold,color: Colors.white),),
                     SizedBox(
                       height:200,
                       width: double.maxFinite,
                       child: ListView.builder(
                           itemCount: _weatherProvider.weatherForcast!.list!.length,
                           scrollDirection: Axis.horizontal,
                           itemBuilder: (context,index){
                             final forecast=_weatherProvider.weatherForcast!.list![index];
                         return Card(
                           margin: EdgeInsets.all(4.0),
                           elevation: 10,

                           child: Padding(
                             padding: const EdgeInsets.all(8.0),
                             child: Column(
                               mainAxisAlignment: MainAxisAlignment.center,
                               children: [
                                 Text(getFormatedDate(forecast.dt!,'EEEE, HH:mm a',),style: TextStyle(fontWeight: FontWeight.bold),),
                                 Image.network('$iconPrefix${forecast.weather![0].icon}$IconSufix'
                                   ,height: 80,
                                   width: 80,),
                                 Text('${forecast.main!.tempMax!.round()}/${forecast.main!.tempMin!.round()}\u00B0'),

                                 SizedBox(height: 5,),
                                 Text('${forecast.weather![0].description}',style: TextStyle(fontStyle: FontStyle.italic),)
                               ],
                             ),
                           ),
                         );
                       }),
                     )

                   ],
                 )
               ],
             ),
           )

        ],
      )
      :
      Center(child: CircularProgressIndicator()),
    );
  }

  num getKm(){
      return vi!/1000;
  }
}
