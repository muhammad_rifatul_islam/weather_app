

import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:geolocator/geolocator.dart';
import 'package:weather_app/models/weather_forcast.dart';
import 'package:weather_app/models/weather_model.dart';
import 'package:weather_app/utils/constants.dart';
import 'package:http/http.dart' as Http;

class WeatherProvider with ChangeNotifier{

   late Position position;
   WeatherModel? _weatherModel;
   WeatherForcast? _weatherForcast;


   WeatherModel? get weatherModel => _weatherModel;
   WeatherForcast? get weatherForcast => _weatherForcast;



   void  setPosition(Position pos){
     position=pos;
   }




   Future<void> getWeatherData() async{
   String url='https://api.openweathermap.org/data/2.5/weather?lat=${position.latitude}&lon=${position.longitude}&units=metric&appid=$apiKey';

   try{

           final response=await Http.get(Uri.parse(url));
             final map=json.decode(response.body);
         if(response.statusCode==200){
           _weatherModel= WeatherModel.fromJson(map);
           notifyListeners();
         }
         else{
           Fluttertoast.showToast(msg: '${response.statusCode}',
            gravity: ToastGravity.CENTER,
             backgroundColor: Colors.red,
           textColor: Colors.white);
         }

   }catch(error){
     throw error;
   }

 }

   Future<void> getWeatherForecastData() async{
     String url='https://api.openweathermap.org/data/2.5/forecast?lat=${position.latitude}&lon=${position.longitude}&units=metric&appid=$apiKey';

     try{

       final response=await Http.get(Uri.parse(url));
       final map=json.decode(response.body);
       if(response.statusCode==200){
         _weatherForcast= WeatherForcast.fromJson(map);
         notifyListeners();
       }
       else{
         Fluttertoast.showToast(msg: '${response.statusCode}',
             gravity: ToastGravity.CENTER,
             backgroundColor: Colors.red,
             textColor: Colors.white);
       }

     }catch(error){
       throw error;
     }

   }

}